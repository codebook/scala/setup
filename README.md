# Dev Environment Setup Guide (Debian/Ubuntu)


You can install `sbt` (Scala Build Tool) via apt:

    echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
    sudo apt update && sudo apt install -y sbt


SBT getting started guide:

* http://www.scala-lang.org/documentation/getting-started-sbt-track/getting-started-with-scala-and-sbt-on-the-command-line.html



## Test installation

sbt sbtVersion
sbt about




sbt new scala/hello-world.g8

sbt new scala/scala-seed.g8

sbt new scala/scalatest-example.g8




